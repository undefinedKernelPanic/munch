require "manchkin/version"
require "manchkin/support/id_generator"
require "manchkin/card"
require "manchkin/player"
require "manchkin/game"
require "manchkin/deck"
require "manchkin/logger"
require "manchkin/battle"


module Manchkin

  class NonSupportedExtension < StandardError
    def initialize(version)
      super "Extension #{version.to_s} not supported"
    end
  end

  class DefinitionProxy

    attr_reader :cards

    def initialize
      @cards = []
    end

    def card(card_class, count_in_deck = 1, &block)
      card_factory = CardFactory.new
      if block_given?
        card_factory.instance_eval &block
      end
      (1..count_in_deck).each do
        card_instance = Object.const_get("::Manchkin::#{card_class.capitalize}").allocate

        card_factory.attributes.each do |attr_name, attr_val|
          card_instance.instance_variable_set :"@#{attr_name}", attr_val
          card_instance.class.class_eval { send :attr_reader, :"#{attr_name}" }
        end

        card_instance.instance_variable_set :@card_type, card_instance.card_type
        card_instance.instance_variable_set :@subtype, card_instance.class.to_s.gsub('Manchkin::', '').to_sym.downcase
        card_instance.instance_variable_set :@id, IdGenerator.generate_id

        @cards.push card_instance
      end
    end

  end

  class CardFactory < BasicObject
    def initialize
      @attributes = {}
    end

    attr_reader :attributes

    def method_missing(name, *args, &block)
      attributes[name] = args[0]
    end
  end


  SUPPORT_EXTENSIONS = [:classic, :unnatural_axe, :clerical_errors]

  class << self
    attr_accessor :loaded_cards
  end

  def self.new_game(version = :classic)
    load_cards version
    deck = Deck.new @loaded_cards
    Manchkin::Game.new deck
  end

  # def self.new_test_game(version = :classic)
  #   load_cards version
  #   deck = Deck.new @loaded_cards
  #   game = Manchkin::Game.new deck
  #   game.add_player()
  # end

  @tmp_cards = []

  def self.define_deck(deck_name, previous_extension = nil, &block)
    definition_proxy = DefinitionProxy.new
    definition_proxy.instance_eval(&block)

    unless previous_extension.nil?
      @tmp_cards += definition_proxy.cards
      require "manchkin/decks/#{previous_extension.to_s}"
    else
      Manchkin.loaded_cards = @tmp_cards + definition_proxy.cards
    end
  end

  private

  def self.load_cards(version)
    if SUPPORT_EXTENSIONS.include? version
      require "manchkin/decks/#{version.to_s}"
    else
      raise NonSupportedExtension, version
    end
  end

end