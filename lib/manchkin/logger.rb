module Manchkin
  class Logger

    attr_reader :messages, :observers

    def initialize
      @observers = []
      @messages = []
    end

    def add_observer(observer)
      @observers.push observer
    end

    def push(text, player_id = nil)
      message = ::Manchkin::Message.new text, player_id
      @messages.push message
      notify_observers message
    end

    def update(json_object, player_id)
      update = Manchkin::Update.new json_object, player_id
      @messages.push update
      notify_observers update
    end

    def action(action, *args)
      action = ::Manchkin::Action.new action, args
      @messages.push action
      notify_observers action
    end

    def notify_observers(message)
      @observers.each { |observer| observer.update(message) }
    end
  end

  class Update
    attr_reader :object, :player_id, :type
    def initialize(object, player_id = nil)
      @object = object
      @player_id = player_id
      @type = player_id.nil? ? :broadcast : :private

    end
  end

  class Action
    attr_reader :action, :type, :player_id, :params, :id
    def initialize(action, args = {}, player_id = nil)
      @id = object_id
      @action = action
      @params = args.first
      @player_id = player_id
      @type = player_id.nil? ? :broadcast : :private

    end
  end

  class Message

    attr_reader :id, :text, :type, :player_id

    def initialize(text, player_id = nil)
      @id = object_id
      @text = text
      @type = player_id.nil? ? :broadcast : :private
      @player_id = player_id
    end

  end
end