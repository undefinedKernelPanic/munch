require 'manchkin/support/callbackable_methods'
require 'manchkin/states/game'
require 'manchkin/support/to_json'



module Manchkin
  class Game
  
  	include Support::CallbackableMethods
    include Manchkin::Support::StateTransitionCallbacks
    include Manchkin::Support::ToJSON

    TO_JSON_FIELDS_OWNER = [
        { players: -> (d) { players.map { |pl| pl.to_json }.to_json } },
        { battle: -> (d) { battle.to_json } }

    ]

    TO_JSON_FIELDS_OTHER = [
        :id,
        :name
    ]


    attr_reader :players, :logger, :current_player, :battle, :state, :deck
    
  	callbackable_methods_set_defaults delegate_to: :state
      
   	callbackable_method_define name: :add_player
    callbackable_method_define name: :start_game
    callbackable_method_define name: :open_door
    callbackable_method_define name: :try_to_leave
    callbackable_method_define name: :play_curse_on_player
    callbackable_method_define name: :play_modificator_on_battle_char



    def initialize(deck)
      @id = IdGenerator.generate_id
      @deck = deck
      @players = []
      @current_player = nil
      @battle = nil
      @state = Manchkin::GameNew.instance
      @logger = Manchkin::Logger.new

    end
    
    # logger --------------------------------------------------------------
    
    def add_logger_observer(observer)
      @logger.add_observer observer
    end
    
    def notification_message(text, player_id = nil)
      @logger.push text, player_id
    end

    def update_message(json_object, player_id = nil)
      # p 'update'
      @logger.update json_object, player_id
    end
    
    # //logger ------------------------------------------------------------

    def set_battle(battle_inst)
      @battle = battle_inst
    end
    
    def cards
        @deck.cards
    end

    def player_id(id)
      @players.find { |player| player.id == id } || nil
    end
    
    def player(player_name) # find player by name
      @players.find { |player| player.name == player_name } || raise("player #{player_name} not found")
      # player = @players.find { |player| player.name == player_name }
      # raise "player #{player_name} not found" if player.nil?
      # player
    end

    def other_players # all, except current player
      @players.select { |player| player != @current_player }
    end


    def throw_dice
      rand(1..6)
      # 3
    end


    def i_win(player)
      @battle.i_win self, player
    end
    # private
    #
    # def change_state(state)
    #   @state = state
    # end

    # def can_perform_action?(method)
    #   @state.class.instance_methods(false).include? method
    # end

  end
end