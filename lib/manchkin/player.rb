require 'manchkin/support/callbackable_methods'
require 'manchkin/states/player'
require 'manchkin/player_modules/stats'
require 'manchkin/player_modules/player_curses'
require 'manchkin/support/delegator'
require 'manchkin/support/to_json'
require "manchkin/support/dinamic_actions"
require "manchkin/support/state_transition_callbacks"


module Manchkin
    class Player

        include Manchkin::Support::CallbackableMethods
        include Manchkin::Support::Delegator
        include Manchkin::Support::ToJSON
        include Manchkin::Support::DinamicActions
        include Manchkin::Support::StateTransitionCallbacks

        TO_JSON_FIELDS_OWNER = [
            :id,
            :name,
            :sex,
            :maximum_big_gear,
            :maximum_cards_at_end_turn,
            :race_name,
            :klass_name,
            { state: -> (d) { state.name } },            
            :level,
            :power,
            { tem_effect: -> (d) { temporary_effects.active_effects.map { |eff| { desc: eff.description, amount: eff.amount } } } },
            { curses: -> (d) { curses.curses.map { |curse| { name: curse.name, desc: curse.description } } } },
            { equipped: -> (d) { equipped.map { |pl_card| pl_card.card.id } } },
            { carried: -> (d) { carried.map { |pl_card| pl_card.card.id } } },
            { hand: -> (d) { hand.map { |pl_card| pl_card.card.id } } },
            :available_actions
            
        ]
            
        TO_JSON_FIELDS_OTHER = [
            :id,
            :name,
            :power,
            :state
        ]


        callbackable_methods_set_defaults delegate_to: :state

        callbackable_method_define name: :open_door
        callbackable_method_define name: :play_curse_on_player
        callbackable_method_define name: :looking_for_trouble
        # callbackable_method_define name: :equip_gear
        callbackable_method_define name: :play_card_on_self
        callbackable_method_define name: :put_card_to_desk
        # callbackable_method_define name: :equip_klass
        callbackable_method_define name: :try_to_leave

        callbackable_method_define name: :start_turn do |player|
            player.change_state ::Manchkin::PlayerOpenDoor.instance
        end

        callbackable_method_define name: :start_battle do |player, args|
            player.change_state ::Manchkin::PlayerInBattle.instance
        end

        # callbackable_method_define name: : do |player, args|
        #     player.change_state ::Manchkin::PlayerInBattle.instance
        # end


        callbackable_method_define name: :drew_curse do |player, args|
            cc = args.first
            player.receive_curse cc, args.last
        end

        def receive_curse(curse_card, state_transinion_to)
            after_dial_with_curse_transition_to = state_transinion_to
            curses.add_curse self, curse_card
            # before_state = after_dial_with_curse_transition_to
            add_callback :instead, :after_deal_with_curse do
                change_state after_dial_with_curse_transition_to
            end
            change_state ::Manchkin::PlayerBeforeSufferCurse.instance
        end

        callbackable_method_define name: :confirm_curse

        callbackable_method_define name: :after_deal_with_curse
        callbackable_method_define name: :play_modificator_on_battle_char

        # callbackable_method_define name: :play_card_on_player

        callbackable_method_define name: :win_battle do |player, args|
            args.first > args.last
        end




        
        # def change_state(state)
        #     @state = state
        # end

        # def play_card_on_self(card_id)
        #     @state.play_card_on_self self, card_id
        # end

        
        
        
        delegate_methods :drop_card, :add_to_hand, :add_cards, :equip, :put_to_desk,
                         :equipped, :carried, :power, :level, :hand,
                         :race_name, :race, :temporary_effects,
                         :sex, :sex=, :klass_name, :klass, :set_level,
                         to: :stats
        delegate_methods :notification_message, to: :game

        # def klass
        #     @stats.klass
        # end

        attr_reader :stats, :state, :name, :id, :game, :curses
        
        # def notification_message(text, player_id = nil)
        #     p 'PLAYER'
        #     p text, player_id
        # end

        def initialize(uniq_name, sex)
            @id = IdGenerator.generate_id
            @game = nil
            @name = uniq_name
            @stats = Manchkin::PlayerModules::PlayerStats.new self, sex
            @state = ::Manchkin::PlayerInGame.instance
            @curses = Manchkin::PlayerModules::PlayerCurses.new self
            @choose_dialog = nil
        end


        callbackable_method_define name: :maximum_big_gear do |player|
            player.stats.maximum_big_gear
        end

        callbackable_method_define name: :maximum_cards_at_end_turn do |player|
            player.stats.maximum_cards_at_end_turn
        end
        
        def attach_to_game(game)
            @game = game
        end
        
        # def play_card_on_self(card_id)
        #     # p card_id
        #     card = @stats.find_player_card_by_card_id card_id
            
        #     # p card
        #     @stats.equip card
        # end

        def play_on_battle(*args)
            arguments = args.first
            card_id = arguments['card_id']
            recipient_id = arguments['recipient_id']

            player_card = stats.find_player_card_by_card_id card_id

            case player_card.subtype
                when :modificator
                    play_modificator_on_battle_char recipient_id, player_card
                else
            end


            end

        def play_card_on_player(*args)
            arguments = args.first
            card_id = arguments['card_id']
            recipient_id = arguments['recipient_id']
            # p '4444444444444444444444444444'
            # p card_id
            # p recipient_id
            player_card = stats.find_player_card_by_card_id card_id

            case player_card.subtype
                when :curse
                    play_curse_on_player recipient_id, player_card
                else
                    notification_message 'нельзя сыграть на другого игрока', @id
            end

            # notification_message args, @id
        end

        callbackable_method_define name: :throw_dice_for_leave_monster
        # do |player|
        #     player.game.throw_dice
        # end

        
        def available_actions
            # []
            (available_dinamic_actions_names + @state.actions).uniq
        end


        
    end

end