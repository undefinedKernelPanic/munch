Manchkin.define_deck :classic do
    # card :gear, 4 do
    #     name 'Кинжал измены'
    #     gear_type :weapon
    #     size :small
    #     hand 1
    #     bonus 3
    #     price 400
    #     e = Proc.new do
    #         action_if_equip :some_action do |player, card_source_of_action, monster_char_id|
    #             p 'some_action!!!!'
    #         end
    #     end
    #     additions e
    # end
    #
    # card :gear, 4 do
    #     name 'совсехсторонний щит'
    #     gear_type :weapon
    #     size :big
    #     hand 1
    #     bonus 4
    #     price 600
    #     e = Proc.new do
    #         action_if_equip :some_action do |player, card_source_of_action, monster_char_id|
    #             p 'some_action!!!!'
    #         end
    #     end
    #     additions e
    # end
    #
    # card :gear, 4 do
    #     name 'мифрильный панцирь'
    #     gear_type :chest
    #     size :big
    #     bonus 3
    #     price 600
    #     e = Proc.new do
    #         action_if_equip :some_action do |player, card_source_of_action, monster_char_id|
    #             p 'some_action!!!!'
    #         end
    #     end
    #     additions e
    # end
    #
    # card :gear, 8 do
    #     name 'башмаки реально быстрого бега'
    #     gear_type :boots
    #     bonus 3
    #     price 400
    #     size :small
    #     # e = Proc.new do
    #     #     only klass: :mage
    #     # end
    #     # restrictions e
    #     e = Proc.new do
    #         action_if_equip :some_action do |player, card_source_of_action, monster_char_id|
    #             player.test_action
    #         end
    #     end
    #     additions e
    # end

    # card :klass, 8 do
    #     klass_name :warrior
    #     name :warrior
    #     e = Proc.new do
    #         ability :rage
    #         ability :winner
    #     end
    #     abilities e
    # end



    # card :race, 8 do
    #     name :elf
    #     race_name :elf
    #     e = Proc.new do
    #         callback :instead, :maximum_big_gear, is_permanent: true do
    #             500
    #         end
    #     end
    #     race_abilities e
    #
    # end

    # card :curse, 18 do
    #     name 'смена пола'
    #     description 'очень страшное проклятие уууу'
    #     # curse do |player|
    #     e = Proc.new do |player|
    #
    #         if player.sex == :male
    #             player.sex = :female
    #         else
    #             player.sex = :male
    #         end
    #         # before_transition to: Manckin::PlayerInBattle.instance do |pl|
    #         temporary_effect 'смятение', -4, state: Manchkin::PlayerInGame.instance
    #     end
    #     activate e
    #
    #     d = Proc.new do |player, player_curse|
    #         after_transition from: Manchkin::PlayerOpenedDoor.instance do
    #             # p 'TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT'
    #             player.curses.deactivate player_curse.id
    #         end
    #     end
    #     deactivate d
    # end

    
    # card :monster, 18 do
    #     name '3872 орка'
    #     level 10
    #     treasures 3
    #     e = Proc.new do
    #         have_effect 6, race: :dwarf
    #         # have_effect -9, race: :human
    #     end
    #     additions e
    # end

    # card :potion, 8 do
    #     name 'хотельное кольцо'
    # end

#     -------------------------------------------------------------------
    card :gear, 1 do
        name 'Баклер бахвала'
        bonus 2
        price 400
        size :small
        gear_type :weapon
        hand 1
    end

    card :gear, 1 do
        name 'Башмаки могучего пенделя'
        bonus 3
        price 400
        size :small
        gear_type :boots
    end

    card :gear, 1 do
        name 'Башмаки реально быстрого бега'
        desc 'У тебя +2 на смывку.'
        bonus 0
        price 400
        size :small
        gear_type :boots
        e = Proc.new do
            after :throw_dice_for_leave_monster do |player, res|
                res + 2
            end
        end
        additions e
    end

    card :gear, 1 do
        name 'Бензопила кровавого расчленения'
        bonus 0
        price 600
        size :big
        gear_type :weapon
        hand 2
    end

    card :gear, 1 do
        name 'Дуб джентельменов'
        desc 'Только для мужчин.'
        bonus 3
        price 400
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only sex: :male
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Кинжал измены'
        desc 'Только для воров.'
        bonus 3
        price 400
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only klass: :thief
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Кожаный прикид'
        bonus 1
        price 200
        size :small
        gear_type :chest
    end

    card :gear, 1 do
        name 'Колготки великанской силы'
        bonus 3
        price 600
        size :small
        gear_type :pants
        e = Proc.new do
            except klass_name: :warrior
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Коротыширокие латы'
        desc 'Только для дварфов.'
        bonus 3
        price 400
        size :small
        gear_type :chest
        e = Proc.new do
            only race_name: :dwarf
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Крыса на палочке'
        desc 'Лучше, чем ничего! Можеш сбросить крысу, даже если не используешь её,
              чтобы автоматически смыться от любого монствра не выше 8-го уровня.'
        bonus 3
        price 0
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
    #TODO работать должно даже если просто carry ее
            action_if_equip :drop_rat, state: Manchkin::PlayerInBattle.instance do |player, card_source_of_action, monster_char_id|
                player.notification_message 'drop_raaaaaat', player.id
            end
        end
        additions e
    end

    card :gear, 1 do
        name 'Лучок с ленточками'
        desc 'Только для эльфов.'
        bonus 4
        price 800
        size :small
        gear_type :weapon
        hand 2
        e = Proc.new do
            only race_name: :elf
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Меч песни и пляски'
        desc 'Не для воров.'
        bonus 2
        price 400
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            except klass_name: :thief
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Меч широты вглядов'
        desc 'Только для женщин.'
        bonus 3
        price 400
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only sex: :female
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Мифрильный панцирь'
        desc 'Не для волшебников.'
        bonus 3
        price 600
        size :big
        gear_type :chest
        e = Proc.new do
            except klass_name: :wizard
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Коленоотбойный молот'
        desc 'Только для дварфов.'
        bonus 4
        price 600
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only race_name: :dwarf
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Огромная каменюга'
        bonus 3
        price 0
        size :big
        gear_type :weapon
        hand 2
    end

    card :gear, 1 do
        name 'Одиннадцатифутовый шест'
        bonus 1
        price 200
        size :small
        gear_type :weapon
        hand 2
    end

    card :gear, 1 do
        name 'Остроконечная шляпа могущества'
        bonus 3
        price 400
        size :small
        gear_type :head
    end

    card :gear, 1 do
        name 'Наколенники развода'
        desc 'Не для клириков. Ни один игрок, чей уровень выше твоего, не может отказать тебе помочь в бою и не требует за это награды.
              Ты не можешь получить победный уровень за бой, в котором твой помощник разведён наколенниками.'
        bonus 0
        price 600
        size :small
        gear_type :knee
        e = Proc.new do
            action_if_equip :help_me, state: Manchkin::PlayerInBattle.instance do |player, player_helper_id|
                player.notification_message 'help_me', player.id
            end
        end
        additions e
        a = Proc.new do
            except klass_name: :cleric
        end
        restrictions a
    end

    card :gear, 1 do
        name 'Палица остроты'
        desc 'Только для клириков.'
        bonus 4
        price 600
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only klass_name: :cleric
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Плащ замутнённости'
        desc 'Только для воров'
        bonus 4
        price 600
        size :small
        gear_type :cloak
        hand 1
        e = Proc.new do
            only klass_name: :thief
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Меч коварного ублюдка'
        bonus 2
        price 400
        size :small
        gear_type :weapon
        hand 1
    end

    card :gear, 1 do
        name 'Посох напалма'
        desc 'Только для волшебников.'
        bonus 5
        price 800
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only klass_name: :wizard
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Пылающие латы'
        bonus 2
        price 400
        size :small
        gear_type :chest
    end

    card :gear, 1 do
        name 'Рапира такнечестности'
        desc 'Только для эльфов.'
        bonus 3
        price 600
        size :small
        gear_type :weapon
        hand 1
        e = Proc.new do
            only race_name: :elf
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Реально впечатляющий титул'
        bonus 3
        price 0
        size :small
        gear_type :title
    end

    card :gear, 1 do
        name 'Сандалеты-протекторы'
        desc 'Защищают тебя от проклятий, которые ты вытягиваешь, вышибая двери.
                  Не спасут от проклятий, сыгранных на тебя другими игроками.'
        bonus 0
        price 700
        size :small
        gear_type :boots
        e = Proc.new do
            instead :drew_curse do |player|
                player.notification_message "#{player.name} избежал проклятия"
            end
        end
        additions e
    end

    card :gear, 1 do
        name 'Слизистая оболочка'
        bonus 1
        price 200
        size :small
        gear_type :chest
    end

    card :gear, 1 do
        name 'Совсехсторонний щит'
        desc 'Только для воинов'
        bonus 4
        price 600
        size :big
        gear_type :weapon
        hand 1
        e = Proc.new do
            only klass_name: :warrior
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Бандана сволочизма'
        desc 'Только для людей'
        bonus 3
        price 400
        size :small
        gear_type :head
        e = Proc.new do
            only race_name: :human
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Боевая стремянка'
        desc 'Только для хафлингов'
        bonus 3
        price 400
        size :small
        gear_type :ladder
        e = Proc.new do
            only race_name: :halfling
        end
        restrictions e
    end


    card :gear, 1 do
        name 'Сыротёрка-умиротворения'
        desc 'Только для клириков'
        bonus 3
        price 400
        size :small
        gear_type :weapon
        head 1
        e = Proc.new do
            only klass_name: :cleric
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Швейцарская армейская алебарда'
        desc 'Только для людей'
        bonus 4
        price 600
        size :big
        gear_type :weapon
        hand 2
        e = Proc.new do
            only race_name: :human
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Чарующая дуда'
        desc 'Этот мелодичный инструмент завораживает врагов: у тебя +3 на смывку. Успешно смывшись, можешь взять
                 одно сокровище в закрытую (только один раз в ход)'
        bonus 0
        price 300
        size :big
        gear_type :weapon
        hand 1
    end

    card :gear, 1 do
        name 'Шлем отваги'
        # desc 'Этот мелодичный инструмент завораживает врагов: у тебя +3 на смывку. Успешно смывшись, можешь взять
        #          одно сокровище в закрытую (только один раз в ход)'
        bonus 1
        price 200
        size :small
        gear_type :head
    end

    card :gear, 1 do
        name 'Шлем-рогач'
        desc 'Бонус для эльфов: +3'
        bonus 1
        price 600
        size :small
        gear_type :head
        e = Proc.new do
            bonus 'Шлем-рогач', 3, race_name: :elf
        end
        additions e
    end

    card :gear, 1 do
        name 'Сэндвич с сыром и селёдкой «Душистая смерть»'
        desc 'Только для хафлингов'
        bonus 3
        price 400
        size :small
        gear_type :sandwich
        e = Proc.new do
            only race_name: :halfling
        end
        restrictions e
    end

    card :gear, 1 do
        name 'Шипастые коленки'
        bonus 1
        price 200
        size :small
        gear_type :knee
    end

#     -----------------races---------------------------------
#     card :race, 18 do
#         name :dwarf
#         race_name :dwarf
#         e = Proc.new do
#             callback :instead, :maximum_big_gear, is_permanent: true do
#                 500
#             end
#             callback :instead, :maximum_cards_at_end_turn, is_permanent: true do
#                 6
#             end
#         end
#         race_abilities e
#
#     end
#
#     card :race, 18 do
#         name :elf
#         race_name :elf
#         e = Proc.new do
#             callback :after, :throw_dice_for_leave_monster, is_permanent: true do |player, res|
#
#                 res + 1
#             end
#
#         end
#         race_abilities e
#
#     end
#     -----------------races---------------------------------


#     ----------------monsters----------------------------------


    card :monster, 18 do
        name 'Мадемонуазели'
        level 8
        treasures 2
        desc 'В бою с ними никакие бонусы не помогут. Срази их размером уровня.'
        bad_stuff_desc 'Твой уровень падает до уровня самого низкоуровневого манчкина в игре.'
        e = Proc.new do |ch_pls|
            ch_pls.each do |pl_ch|
                id = pl_ch.add_callback :instead, :power, is_permanent: true do |ch|
                    ch.player.level
                end
                pl_ch.battle.after_transition from: Manchkin::BattleInProgress.instance do |o|
                    pl_ch.remove_callback id
                end
            end
        end
        additions e
        e = Proc.new do |player|
            less_level = player.battle.players.inject(10) do |res, pl|
                l = pl.level
                return l if l < res
            end
            player.set_level less_level

        end
        bad_stuff e
    end

    # card :monster, 18 do
    #     name '3872 орка'
    #     level 10
    #     treasures 3
    #     e = Proc.new do |battle|
    #         have_effect 6, race: :dwarf
    #         # have_effect -9, race: :human
    #     end
    #     additions e
    #     e = Proc.new do |player|
    #         id = player.add_dinamic_action :throw_dice do
    #             n = player.game.throw_dice
    #             if n <= 2
    #               player.notification_message "#{player.name} затоптан до смерти"
    #             else
    #               player.notification_message "#{player.name} потерял #{n} уровней"
    #             end
    #             player.remove_dinamic_action id: id
    #         end
    #     end
    #     bad_stuff e
    # end
#     ----------------monsters----------------------------------


    #--------------battle modificators
    # card :modificator, 18 do
    #     name 'Громадный'
    #     desc '+10 к монстру. Играй в любой бой. За побеждённого громадного монстра тяни на два сокровища больше.'
    #     bonus 10
    #     extra_treasures 2
    # end
    #
    # card :modificator, 18 do
    #     name 'Престарелый'
    #     desc '+10 к монстру. Играй в любой бой. За побеждённого престарелого монстра тяни на два сокровища больше.'
    #     bonus 10
    #     extra_treasures 2
    # end
    #
    # card :modificator, 18 do
    #     name 'Умудрённый'
    #     desc '+5 к монстру. Играй в любой бой. За побеждённого умудрённого монстра тяни на одно сокровище больше.'
    #     bonus 5
    #     extra_treasures 1
    # end
    #
    # card :modificator, 18 do
    #     name 'Разъярённый'
    #     desc '+5 к монстру. Играй в любой бой. За побеждённого разъярённого монстра тяни на одно сокровище больше.'
    #     bonus 5
    #     extra_treasures 1
    # end
    #
    # card :modificator, 18 do
    #     name 'Грудной'
    #     desc '-5 к монстру. Играй в любой бой. За побеждённого грудного монстра тяни на одно сокровище меньше (но не меньше одного).'
    #     bonus -5
    #     extra_treasures -1
    # end
    #--------------battle modificators

    #---------------potions----------------
    # card :potion, 1 do
    #     name 'Зелье холодильного взрыва'
    #     desc 'Играй в любой бой. +3 любой стороне. Разовая шмотка.'
    #     bonus 3
    #     price 100
    # end
    #
    # card :potion, 1 do
    #     name 'Пелье зутаницы'
    #     desc 'Играй в любой бой. +3 любой стороне. Разовая шмотка.'
    #     bonus 3
    #     price 100
    # end
    #
    # card :potion, 1 do
    #     name 'Зелье идиотской храбрости'
    #     desc 'Играй в любой бой. +2 любой стороне. Разовая шмотка.'
    #     bonus 3
    #     price 100
    # end
    #
    # card :potion, 1 do
    #     name 'Зелье пламенной отравы'
    #     desc 'Играй в любой бой. +3 любой стороне. Разовая шмотка.'
    #     bonus 3
    #     price 100
    # end

    #---------------potions----------------


    #-----------------lvlup--------------------

    card :lvlup, 1 do
        name '1000 голдов'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Примени непонятные правила'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Удобная ошибка при сложении'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Зелье крутизны'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Поглумись над трупами'
        desc 'Получи уровень. Играй только после боя, но необязательно после своего.'
        e = Proc.new do

        end
        conditions e
    end

    card :lvlup, 1 do
        name 'Поплачься мастеру'
        desc 'Получи уровень. Не можешь играть эту карту, если ты самый высокоуровневый манчкин (или один из таких).'
        e = Proc.new do

        end
        conditions e
    end

    card :lvlup, 1 do
        name 'Прикорми мастера'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Пришей наёмничка'
        desc 'Получи уровень. Играй только в том случае, если у кого-нибудь есть наёмничек. Сбрось из игры любого наёмничка на свой выбор. Шмотки, которые он нёс, остаются у его хозяина.'
        e = Proc.new do

        end
        conditions e
    end

    card :lvlup, 1 do
        name 'Вскипяти муравейник'
        desc 'Получи уровень.'
    end

    card :lvlup, 1 do
        name 'Укради уровень'
        desc 'Получи уровень. Выбранный тобой соперник теряет уровень.'
    end

    #-----------------lvlup--------------------




end
