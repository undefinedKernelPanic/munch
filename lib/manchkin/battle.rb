require "manchkin/support/temporary_effect"
require 'manchkin/support/to_json'
require 'manchkin/support/delegator'
require "manchkin/support/state_transition_callbacks"
require 'manchkin/states/battle'



module Manchkin
  class Battle

    include Manchkin::Support::ToJSON
    include Manchkin::Support::CallbackableMethods
    include Manchkin::Support::Delegator
    include Manchkin::Support::StateTransitionCallbacks



    callbackable_methods_set_defaults delegate_to: :state

    callbackable_method_define name: :try_to_leave
    callbackable_method_define name: :i_win
    callbackable_method_define name: :play_modificator_on_battle_char


    delegate_methods :notification_message, :update_message, to: :game


    TO_JSON_FIELDS_OWNER = [
        :winner,
        { player_win: -> (d) { winner == :manchkin } },
        { monster_win: -> (d) { winner == :monster } },
        { players: -> (d) { players.map { |pl_ch| { id: pl_ch.id, name: pl_ch.name, power: pl_ch.power } } } },
        { monsters: -> (d) { monsters.map { |mn_ch| { id: mn_ch.id, name: mn_ch.name, power: mn_ch.power, card_id: mn_ch.card.id, effects: mn_ch.temporary_effects.map {|eff| {desc: eff.description, amount: eff.amount }} } } } },
        { player_power: -> (d) { players.inject(0) { |res, pl_ch| res + pl_ch.power } } },
        { monster_power: -> (d) { monsters.inject(0) { |res, mn_ch| res + mn_ch.power } } }
    ]

    attr_reader :temporary_effects, :players, :monsters, :game, :state

    def initialize(game, player, monster_card)
      @game = game
      @players = []
      @monsters = []
      @temporary_effects = Manchkin::Support::TemporaryEffects.new self
      @state = Manchkin::BattleInProgress.instance


      #для разделения верменных бонусов разовых шмоток
      @players_side = IdGenerator.generate_id
      @monsters_side = IdGenerator.generate_id

      add_player player, :main
      add_help_action_to_other_players player
      # add_try_to_leave_to_player player
      add_monster monster_card
    end

    def winner
      players_power = players.inject(0) { |res, pl_ch| res + pl_ch.power }
      monsters_power = monsters.inject(0) { |res, mn_ch| res + mn_ch.power }

      res = false
      @players.each do |pl_char|
        res ||= pl_char.player.win_battle players_power, monsters_power
      end
      res ? :manchkin : :monster
    end

    def add_player(player, role = :helper)
      @players.push Manchkin::PlayerCharacter.new player, self, role
    end

    def add_monster(card)
      @monsters.push Manchkin::MonsterCharacter.new card, self
    end


    callbackable_method_define name: :can_add_player do |battle|
      battle.players.size < 2
    end

    # def can_add_player?
    #   @players.size < 2
    # end

    def can_add_monster?
      @monsters.size < 2
    end

    def add_temporary_effect(description, amount, apply_on, conditions = {}, &condition_checker)
      @temporary_effects.add description, amount, apply_on, conditions, &condition_checker
    end

    def remove_temporary_effect(id)
      @temporary_effects.remove_by_id id
    end

    def race
      @players.map { |player_char| player_char.player.race_name.to_sym }
    end

    def klass
      @players.map { |player_char| player_char.player.klass_name.to_sym }
    end

    def player_suffer!(player, monster)
      monster.card.bad_stuff.call player
      # pl_char_by_player(player)
    end

    def pl_char_by_player(player)
      @players.find { |pl| pl.player.id == player }
    end

    private



    # def add_try_to_leave_to_player(player)
    #   id = player.add_dinamic_action :try_to_leave,
    #
    # end

    def add_help_action_to_other_players(main_player)
      if can_add_player
        other_players = @game.players
            .select { |pl| pl.id != main_player.id }

        other_players.each do |pl|
          id = pl.add_dinamic_action :go_to_help do |player|
            add_player player
            other_players.each do |player_rem|
              player_rem.remove_dinamic_action id: id
            end
          end

          after_transition from: Manchkin::BattleInProgress.instance do |battle|
            other_players.each do |player_rem|
              player_rem.remove_dinamic_action id: id
            end
          end

        end
      end

    end



    end

  class MonsterCharacter

    attr_reader :battle, :card, :id, :treasures

    def initialize(card, battle)
      @id = IdGenerator.generate_id
      @battle = battle
      @card = card
      @treasures = card.treasures

      proxy = MonsterProxy.new self, @battle
      proxy.instance_exec battle.players, &@card.additions
    end

    def add_treasures(amount)
      @treasures + amount
      if @treasures < 1
        @treasures = 1
      end
    end

    def name
      @card.name
    end

    def power
      @card.level + monster_temp_effect_amount
    end

    def monster_temp_effect_amount
      @battle.temporary_effects.amount @id
    end

    def temporary_effects
      @battle.temporary_effects.active_effects @id
    end

  end

  class PlayerCharacter

    include Manchkin::Support::Delegator
    include Manchkin::Support::CallbackableMethods


    attr_reader :player, :role, :battle, :id, :available_treasures

    delegate_methods :name, to: :player

    def initialize(player, battle, role = :helper)
      @id = IdGenerator.generate_id
      @player = player
      @role = role
      @battle = battle
      @available_treasures = 0

      player.start_battle
    end

    def set_available_treasures(amount)
      @available_treasures = amount
    end

    callbackable_method_define name: :power do |ch_pl|
      ch_pl.player.power + ch_pl.player_temp_effects_amount
    end

    def player_temp_effects_amount
      0
    end

    def temporary_effects
      @battle.temporary_effects.active_effects @id
    end

    # def add_try_to_leave_action
      # @player.add_dinamic_action :try_to_leave
    # end
  end

  class MonsterProxy

    def initialize(monster_char, battle)
      @monster_char = monster_char
      @battle = battle
    end

    def have_effect(amount, conditions = {}) # :race, :klass, :amount
      @battle.add_temporary_effect 'temp_effect', amount, @monster_char.id, conditions do |k, v|
        @battle.send(k.to_sym).include? v
      end
      # @temporary_effects.add 'temp_effect'
    end
  end

end