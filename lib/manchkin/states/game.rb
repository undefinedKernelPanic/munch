require 'singleton'

module Manchkin
  class GameState
    include Singleton

    # def logger(game, message, players)
    #   game.logger.push
    # end



    def play_curse_on_player(game, sender_id, recipient_id, player_card)
      # p 'after_dial_with_curse_transition_to'
      victim = game.player_id(recipient_id)

      victim.receive_curse player_card.card, victim.state
      # p game.players.map {|pl| pl.to_json}
      # game.update_message(game.players.map {|pl| pl.to_json}.to_json)
    end

  end

  class GameBattle < GameState
    def try_to_leave(game, player)
      game.battle.try_to_leave player
    end

    def play_modificator_on_battle_char(game, player, recipient_id, player_card)
      game.battle.play_modificator_on_battle_char(player, recipient_id, player_card)
      # сдесь бы сбросить player_card
    end
  end

  class GameNew < GameState
    def add_player(game, uniq_name, sex)
      game.players.push Manchkin::Player.new uniq_name, sex
    end

    def start_game(game)
    #   game.add_player 'sol', :male

      game.deck.shuffle!

      game.players.each do |p|
        p.send :add_cards, game.deck.draw_start_hand
        p.send :attach_to_game, game
      end

      game.instance_variable_set "@current_player", game.players.first

      # game.current_player.send :change_state, ::Manchkin::PlayerOpenDoor.instance
      game.send :change_state, ::Manchkin::GameInProgress.instance
      game.current_player.start_turn
      
      game.notification_message 'game is about to begin'
    end
  end

  class GameInProgress < GameState

    def open_door(game, player)
      # logger = game.logger
      card = game.deck.draw_card

      game.other_players.each { |pl| game.notification_message "#{game.current_player.name} вытянул <#{card.id}>", pl.id }
      game.notification_message "тебе досталось <#{card.id}>", player.id

      # player.add_to_hand card
      case card.subtype
        when :curse
          player.drew_curse card, Manchkin::PlayerOpenedDoor.instance
        when :monster
          battle = Manchkin::Battle.new(game, player, card)
          game.change_state Manchkin::GameBattle.instance
          game.set_battle battle
          # player.start_battle
          # game.notification_message battle.to_json.to_s, player.id
        else
          player.add_to_hand card
      end

      # if card.subtype == :curse
      #   player.drew_curse card, Manchkin::PlayerOpenedDoor.instance
      # end
      #
      # if card.subtype == :monster
      #   battle = Manchkin::Battle.new(game, player, card)
      #   game.battle battle
      #   player.start_battle
      #   game.notification_message battle.to_json.to_s, player.id
      # end

      # game.notification_message card.subtype, player.id

      # p 'game open door from state'

    end
  end
end