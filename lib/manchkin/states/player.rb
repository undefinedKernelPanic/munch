require 'singleton'
module Manchkin
	class PlayerState

	include Singleton
	
	
	def method_missing(m, *args, &block)
		player = args.first 
		player.notification_message('no conditions to do this', player.id)
		nil
	end
	
	def name
		self.class.to_s
	end
	
	def actions
		[]
	end

	def play_modificator_on_battle_char(player, recipient_id, player_card)
		player.game.play_modificator_on_battle_char(player, recipient_id, player_card)
	end

	def play_curse_on_player(player, recipient_id, player_card)
		player.game.play_curse_on_player player.id, recipient_id, player_card
	end

	def put_card_to_desk(player, args)
		player_card = player.stats.find_player_card_by_card_id args['card_id']
		if player_card.nil?
			player.notification_message 'нет такой карты', player.id
		else
			player.put_to_desk player_card
		end
	end
	
	def play_card_on_self(player, args)
		# p 'AAAAAAAAAAAAAAAAAAAAAAAAAAA'
		# p args
		player_card = player.stats.find_player_card_by_card_id args['card_id']
		# p player_card.card.subtype
		player.equip player_card
		# if player_card.card.subtype == :gear
		# 	player.equip_gear player_card
		# end
		# if player_card.card.subtype == :klass
		# 	player.equip_klass player_card
		# end
	end

	end

	class PlayerEndTurn < PlayerState

	end

	class PlayerOpenedDoor < PlayerState # если открыл дверь в открытую и не встретил монстра

		def looking_for_trouble(player)
			player.change_state Manchkin::PlayerEndTurn.instance
		end

		def actions
			[:looking_for_trouble]
		end

		def name
			'player_opened_door'
		end
	end


	class PlayerBeforeSufferCurse < PlayerState

		def actions
			[:confirm_curse]
		end

		def name
			'player_before_suffer_curse'
		end

		def confirm_curse(player)
			player.curses.activate
			# player.notification_message 'suffer!'
			player.after_deal_with_curse
		end

		def after_deal_with_curse

		end


	end

	class PlayerInBattle < PlayerState
		def actions
			[:try_to_leave, :i_win]
		end

		def name
			'player in battle'
		end

		def try_to_leave(player)
			player.game.try_to_leave player
		end

		def i_win(player)
			player.game.i_win(player)
		end
	end


	class PlayerTryToLeave < PlayerState
		# def actions
		# 	[]
		# end
		def throw_dice_for_leave_monster(player)
			player.game.throw_dice
		end

	end

	class PlayerLeftMonster < PlayerState

	end

	class PlayerWinMonster < PlayerState

	end

	class PlayerSufferBadStuff < PlayerState

	end

	class PlayerChooseDialog < PlayerState
		def choosen_items(player, items_ids)
			player.chage_state player.choose_dialog
			player.choose_dialog.get_items_by_id items_ids
		end
	end

	class PlayerInGame < PlayerState


		def name
			'player_in_game'
		end
	end

	class PlayerOpenDoor < PlayerState

		def actions
			[:open_door]	
		end

		def name
			'player_open_door'
		end


		
		# def play_card_on_self(player, card_id)
		# 	super player, card_id
		# end
		
		def equip_gear(player, player_card)
			player.equip player_card
		end

		def equip_klass(player, player_card)
			player.equip player_card
		end


		def open_door(player)
			player.game.open_door player
		end

	end
end