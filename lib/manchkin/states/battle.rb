require 'singleton'

module Manchkin
  class BattleState
    include Singleton

    def name
      self.class.to_s
    end
  end

  class BattleInProgress < BattleState

    def play_modificator_on_battle_char(battle, player, battle_char_id, player_card)
      if player_card.subtype == :modificator
        monster = battle.monsters.find { |ch_mn| ch_mn.id == battle_char_id }
        battle.add_temporary_effect player_card.card.desc, player_card.card.bonus, monster.id
        monster.add_treasures player_card.card.extra_treasures

        battle.notification_message "#{player.name} играет <#{player_card.card.id}> на <#{monster.card.id}>"
      else
        battle.notification_message 'можно сыграть только на монстра', player.id
      end
    end

    def i_win(battle, player)
      player.change_state Manchkin::PlayerWinMonster.instance
      ch_pl = battle.pl_char_by_player(player)
      ch_pl.set_available_treasures battle.monsters.first.card.treasures

      # player.add_dinamic_action :draw_treasure do |pl|
        # pl.draw
      # end

    end

    def try_to_leave(battle, player)
      # p 'BattleInProgress < BattleState'
      # battle.update_message battle.to_json, player.id
      player.change_state Manchkin::PlayerTryToLeave.instance
      e = player.throw_dice_for_leave_monster
      if e
      # battle.game.throw_dice
        battle.change_state Manchkin::BattlePlayerTryToLeave.instance
        battle.notification_message "#{e} кинул"

        if e <= 4
          player.change_state Manchkin::PlayerSufferBadStuff.instance
          battle.player_suffer! player, battle.monsters.first


        else
          player.change_state Manchkin::PlayerOpenedDoor.instance

        end
      end
    end
  end

  class BattlePlayerTryToLeave < BattleState

  end

end