require 'manchkin/support/delegator'
require 'manchkin/player_modules/player_card'
require 'manchkin/support/callbackable_methods'
require 'manchkin/player_modules/proxy_classes/restriction'




module Manchkin
    module PlayerModule
        class PlayerCards # все карты игрока
            include Manchkin::Support::Delegator
            include Manchkin::Support::CallbackableMethods

            CARDS_ON_DESK = [:gear, :klass, :race]
            MAXIMUM_BIG_GEAR = 1
            CARDS_AMOUNT_AT_THE_TURN_END = 5
            GEAR_TYPES = [:head, :weapon, :two_handed_weapon, :chest,
                          :pants, :knee, :boots, :cloak, :title, :ladder, :sandwich]

            delegate_methods :notification_message, :klass, :race, to: :player

            attr_reader :player, :all_cards
            
            def initialize(player)
                @player = player
                @all_cards = []
            end

            def drop_card(players_card) # в сброс отправляется
                remove_card hand.first
            end

            def remove_card(players_card)
                pl_cd = @all_cards.delete players_card
                pl_cd.remove_added_methods
            end

            callbackable_method_define name: :can_equip do |player_cards_instance, args|
                players_card = args[0]
                proxy = Manchkin::PlayerModule::RestrictionProxy.new player_cards_instance, players_card
                # проверить кастомные ограничения карты, остальные ограничения в логике RestrictionProxy
                if players_card.card.respond_to? :restrictions
                    pr = players_card.restrictions
                    proxy.instance_eval &pr unless pr.nil?
                end

                #TODO вынести сообщения куданить отсюда
                proxy.messages.each do |message|
                    player_cards_instance.notification_message message, player_cards_instance.player.id
                end
                # p proxy.passed?
                proxy.passed?
            end

            def power
                level + gear_bonuses + temporary_effects
            end
            
            def hand
                card_selector :hand
            end
            
            def equipped
                card_selector :equipped
            end
            
            def carried
                card_selector :carried
            end
            
            def find_player_card_by_card_id(card_id)
                # p @all_cards.map {|i| i.card.id}
                @all_cards.find { |player_card| player_card.card.id == card_id }
            end
            
            def add_cards(cards)
                cards.each do |cd|
                    add_to_hand cd
                end
            end

            #TODO вернуть из классо race & klass carry & equip
            def put_to_desk(player_card)
                case player_card.subtype
                    when :gear
                        player_card.carry!
                    when :klass
                        if player_card == @player.stats.klass.player_card_klass
                            @player.klass.unequip_klass!
                        else
                            player_card.carry!
                        end
                        #
                    when :race
                        if player_card == @player.stats.race.player_card_race
                            @player.race.unequip_race!
                        else
                            player_card.carry!
                        end
                    else
                        notification_message "#{player_card.subtype} нельзя выложить на стол", @player.id
                end


                # if CARDS_ON_DESK.include? player_card.subtype
                #     player_card.carry!
                # else
                #     notification_message "#{player_card.subtype} нельзя выложить на стол", @player.id
                # end
            end
            
            def add_to_hand(card)
                pc = Manchkin::PlayerModule::PlayerCard.new @player, card
                @all_cards.push pc

                # if pc.card.respond_to? :additions
                #     proxy = Manchkin::PlayerModule::AdditionProxy.new @player, pc
                #     proxy.instance_eval &pc.additions unless pc.additions.nil?
                # end
                pc
            end

            def equip(players_card)
                case players_card.subtype
                    when :gear
                        if can_equip players_card
                            players_card.equip!
                        # else
                          # TODO здесь сообщения почему не могу надеть
                        end
                    when :klass
                        # players_card.equip!
                        @player.klass.equip_klass! players_card
                    when :race
                        @player.race.equip_race! players_card
                    else
                        notification_message "#{players_card.subtype} нельзя одеть"
                end

                #
                # if players_card.subtype == :gear
                #     if can_equip players_card
                #         players_card.equip!
                #     end
                # else
                #   if players_card.subtype == :klass
                #       players_card.equip!
                #       @player.klass.equip_klass! players_card
                #   end
                # end
            end



            # callbackable_method_define name: :maximum_big_gear do |stat_instance|
            #     1
            # end

            callbackable_method_define name: :count_equipped_big_gears do |stat_instance|
                stat_instance.equipped
                    .select { |player_card| player_card.card.gear? }
                    .select { |players_card| players_card.card.big? }
                    .size

            end

            #-----------------------------------------------------

            def maximum_big_gear
                MAXIMUM_BIG_GEAR
            end

            def maximum_cards_at_end_turn
                CARDS_AMOUNT_AT_THE_TURN_END
            end

            # def get_gear_bonus_by_type(type)
            #     equipped
            #         .select { |player_card| player_card.card.gear? }
            #         .select { |player_card| player_card.can_equip }
            #         .select { |players_card| players_card.card.gear_type == type }.inject(0) { |res, item| res + item.bonus }
            # end

            callbackable_method_define name: :temporary_effects do |stat_instance|
                stat_instance.player.stats.temporary_effects.amount
            end

            # callbackable_method_define name: :boots_bonus do |stat_instance|
            #     stat_instance.get_gear_bonus_by_type(:boots)
            # end
            #
            # callbackable_method_define name: :chest_bonus do |stat_instance|
            #     stat_instance.get_gear_bonus_by_type(:chest)
            # end
            #
            # callbackable_method_define name: :pants_bonus do |stat_instance|
            #     stat_instance.get_gear_bonus_by_type(:pants)
            # end
            #
            # callbackable_method_define name: :head_bonus do |stat_instance|
            #     stat_instance.get_gear_bonus_by_type(:head)
            # end
            #
            # callbackable_method_define name: :weapon_bonus do |stat_instance|
            #     stat_instance.get_gear_bonus_by_type(:weapon)
            # end

            callbackable_method_define name: :level do |stat_instance|
                # p 'AAAAAAAAAAAAAAAAAAAAAAAAAAAA'
                # p stat_instance.player.stats
                stat_instance.player.stats.level
                # 1
            end

            # callbackable_method_define name: :gear_bonuses do |stat_instance|
            #     stat_instance.chest_bonus + stat_instance.head_bonus +
            #         stat_instance.boots_bonus + stat_instance.weapon_bonus +
            #         stat_instance.pants_bonus
            # end


            # ------------------------gear bonuses------------------
            # объявить каждый тип шмотки как метод, чтоб можно было навесить коллбек

            GEAR_TYPES.each do |gear_type|
                callbackable_method_define name: :"#{gear_type}_bonus" do |stat_instance|
                    stat_instance.get_gear_bonus_by_type(gear_type)
                end
            end

            callbackable_method_define name: :gear_bonuses do |stat_instance|
                stat_instance.equipped
                    .select { |player_card| player_card.card.gear? }
                    .select { |player_card| player_card.is_active }
                    .inject(0) { |res, player_card| res + player_card.card.bonus }
                # GEAR_TYPES.inject(0) do |res, gear_type|
                #     # if stat_instance.respond_to?(:"#{gear_type}_bonus")
                #         res + stat_instance.public_send(:"#{gear_type}_bonus")
                #     # else
                #     #   res
                #     # end
                # end
            end


                # ------------------------gear bonuses------------------

            private
            
            def card_selector(selector) # :hand, :carried, :equipped
                @all_cards.select { |pl_card| pl_card.state == selector }               
            end
            
            
            
        end

    end
end