require 'manchkin/support/delegator'


module Manchkin

  module PlayerModule

    class PlayerKlass


      KLASS_ABILITIES = [:rage, :winner]

      def method_missing(m, *args, &block)
        # p 'missing'
        ab = @abilities.find { |ab| ab.name == m }
        # p m
        # p args
        unless ab.nil?
          ab.call args if ab.available?
        else

        end
      end

      attr_reader :player_card_klass

      def initialize(player)
        @player = player
        @klass_name = 'бомж'
        @abilities = []
        @player_action_ids = []
        @player_callbacks_ids = []
        @player_abilities_ids = []
        @player_card_klass = nil

      end

      def klass
        self
      end

      def klass_name
        @klass_name
      end

      def equip_klass!(player_card)
        # p 'equip klass'

        unless @player_card_klass.nil?
          @player_card_klass.carry!
        end
        @player_card_klass = player_card
        @player_card_klass.equip!


        @klass_name = player_card.card.klass_name
        proxy = AbilityProxy.new self
        proxy.instance_eval &player_card.card.abilities
        proxy.abilities.each do |ab|
          ab = Object.const_get("Manchkin::PlayerModule::#{ab.to_s.capitalize}").new @player
          @abilities.push ab
          if ab.active
            add_method_to_player ab.name, ab.conditions || {}, &ab.imp
          else
            add_instead_callback_to_player ab.name, &ab.imp
          end
        end
        # p 'PPPPPPPPPPPPPPPPPPPPPPPPPPPP'
        # p @player.temporary_effects.effects
        # p 'add'

        # add_method_to_player player_card.card.klass_name do
        #
        # end
      end

      def unequip_klass!

        @player_card_klass.carry!
        @player_card_klass = nil

        @klass_name = 'бомж'
        @abilities = []
        remove_methods_from_player

        remove_callbacks_from_player
        # p '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'

        # p '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111'

      end

      private

      def remove_callbacks_from_player
        @player_callbacks_ids.each do |id|
          @player.remove_callback id: id
        end
      end

      def remove_methods_from_player
        @player_abilities_ids.each do |id|
          @player.remove_dinamic_action id: id
        end
      end

      def add_instead_callback_to_player(method_name, &new_method_imp)
        @player_callbacks_ids.push(@player.add_callback :instead, method_name, true, &new_method_imp)
      end

      def add_method_to_player(name, conditions = {}, &imp)
        # imp = -> (player, card_to_drop) { player.stats.klass.send :rage, card_to_drop }
        # p 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
        @player_abilities_ids.push(@player.add_dinamic_action name, conditions, &imp)
        # p @player.dinamic_actions
      end



    end

    class Ability
      include Manchkin::Support::Delegator

      delegate_methods :notification_message, to: :player

      attr_reader :name, :player, :active, :conditions

      def available?
        if @charges == 0
          notification_message 'кончились заряды', @player.id
          return false
        else
          return true
        end
      end

    end

    class Winner < Ability
      def initialize(player)
        @name = :win_battle
        @player = player
        @active = false
      end

      def imp
        # p '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^6'
        -> (player, player_power, monster_power) { p '00000'; p player_power; p monster_power; player_power >= monster_power }
      end

    end


    class Rage < Ability

      def initialize(player)
        @name = :rage
        @player = player
        @charges = 2
        @active = true
        @conditions = {
            state: ::Manchkin::PlayerInBattle.instance
        }
      end

      def imp
        -> (player, card_to_drop) { player.stats.klass.send :rage, card_to_drop }
      end

      def call(drop_card)
        @player.drop_card drop_card
        effect_id = @player.stats.add_temporary_effect 'rage', 1

        @player.after_transition from: ::Manchkin::PlayerInBattle.instance do |player|
          player.state.remove_temporary_effect effect_id
        end
        @charges = @charges - 1
        # p @player.stats.temporary_effects.amount
        # p drop_card
        # p '+++++++++++++++++++++++++++'
        # p player.stats.temporary_effects
      end
    end

      class AbilityProxy

        attr_reader :abilities

        def initialize(player_klass)
          @player_klass = player_klass
          @abilities = []
        end

        def ability(ability_name)
          @abilities.push ability_name
        end
    end
  end
end