module Manchkin
    module PlayerModule
        class RestrictionProxy

            attr_reader :pass, :messages
            # stat - player_cards_instance
            def initialize(stat, players_card, checker_mode = false)
                @players_card = players_card
                @stat = stat
                @pass = true
                @messages = []
                big_gear_checker
                uniqueness_checker
                # p 'oooooooooooooooo'
                # p @pass
            end

            def except(args = {}) #race, klass, sex
                check = !checker(args)
                @pass = @pass && check
                @messages.push "except #{args.map{ |k, v| v }.join(', ')}" unless check
            end

            def only(args = {}) #race, klass, sex
                check = checker(args)
                @pass = @pass && check
                @messages.push "only for #{args.map{ |k, v| v }.join(', ')}" unless check
            end

            def passed?
                @pass
            end

            private

            def big_gear_checker
                # if @players_card.size == :small
                #     @pass = true
                # end
                if @players_card.card.big? && @stat.count_equipped_big_gears >= @stat.player.maximum_big_gear
                    @pass = false
                    @messages.push "не можешь нести больше #{@stat.player.maximum_big_gear} большой шмотки"
                end
            end

            def uniqueness_checker
                equip = @stat.equipped
                    .select { |player_card| player_card.card.gear? }
                    .select { |item| item.card.gear_type == @players_card.card.gear_type }
                # p 'TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT'
                # p equip.size
                if @players_card.card.gear? && @players_card.gear_type != :weapon
                    if equip.size > 0
                        @pass = false
                        @messages.push 'уже одел шмотку такого типа'
                    end
                else
                    busy_hand = equip.inject(0) { |res, item| res + item.hand }
                    # p busy_hand
                    if (2 - busy_hand) - @players_card.card.hand < 0
                        @pass = false
                        @messages.push 'все руки заняты'
                    end
                end
            end

            def checker(args)
                # p args
                args.each do |k, v|
                    # p @stat.player.public_send(k)
                    # p v
                    return false unless @stat.player.public_send(k) == v
                end
                true
            end
        end

    end
end