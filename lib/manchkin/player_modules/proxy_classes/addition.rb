module Manchkin
    module PlayerModule
        class AdditionProxy

            def initialize(player, players_card)
                # p 'BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB'
            
                @player = player
                @players_card = players_card
            end

            def method_missing(m, *args, &block)
            end

            def after(action_name, &emplementation)
                nil
                # p 'after'
                # p action_name
                # p @player
                # p @player.respond_to? :add_callback
                # @players_card.add_callback_id(@player.add_callback :after, action_name, &emplementation)
            end

            def before(action_name, &emplementation)
                nil
                # @players_card.add_callback_id(@player.add_callback :before, action_name, &emplementation)
            end

            def instead(action_name, &emplementation)
                nil
                # @players_card.add_callback_id(@player.add_callback :instead, action_name, &emplementation)
            end

            def have_action(action_name, conditions = {}, &emplementation)
                # p 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa'
                @players_card.add_action_id @player.add_dinamic_action(action_name, conditions, &emplementation), :any
                # @player.add_dinamic_action :action_name, conditions, &emplementation
            end

        end

        class AdditionCarryProxy < AdditionProxy
            def action_if_carry(action_name, conditions = {}, &emplementation)
                @players_card.add_action_id @player.add_dinamic_action(action_name, conditions, &emplementation), :carried
            end
        end


        class AdditionEquippedProxy < AdditionProxy

            def bonus(desc, amount, conditions = {})
                @players_card.add_temp_effect_id @player.stats.add_temporary_effect(desc, amount, conditions), :equipped
            end

            def action_if_equip(action_name, conditions = {}, &emplementation)
                @players_card.add_action_id @player.add_dinamic_action(action_name, conditions, &emplementation), :equipped
            end


            def after(action_name, &emplementation)
                # p 'after'
                # p action_name
                # p @player
                # p @player.respond_to? :add_callback
                # @player.notification_message 'after', @player.id
                @players_card.add_callback_id(@player.add_callback :after, action_name, is_permanent: true, &emplementation)
            end

            def before(action_name, &emplementation)
                @players_card.add_callback_id(@player.add_callback :before, action_name, is_permanent: true, &emplementation)
            end

            def instead(action_name, &emplementation)
                @players_card.add_callback_id(@player.add_callback :instead, action_name, is_permanent: true, &emplementation)
            end
        end

    end
end