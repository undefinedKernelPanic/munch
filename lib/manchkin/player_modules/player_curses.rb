module Manchkin
  module PlayerModules
    class PlayerCurses

      attr_reader :curses

      def initialize(player)
        @player = player
        @curses = []

      end

      def add_curse(player, card)
        @curses.push Curse.new @player, card
      end

      def inactive_curses
        @curses.select { |curse| !curse.active }
      end

      def activate
        inactive_curses.each do |curse|
          curse.activate!

        end

      end

      def deactivate(id)
        # p @curses.map { |c| c.id }
        # p id
        curse = @curses.find { |curse| curse.id == id }
        @curses.delete(curse).deactivate!
              # .first.deactivate!
      end
    end

    class Curse

      attr_reader :actions, :active, :id, :name, :description

      def initialize(player, card)
        @id = IdGenerator.generate_id
        @player = player
        @card = card
        @name = card.name
        @description = card.description
        @actions = CurseActions.new player
        @active = false
      end

      def activate!
        proxy = CurseProxy.new @player, self

        proxy.instance_exec @player, &@card.activate
        proxy.instance_exec @player, self, &@card.deactivate if @card.respond_to? :deactivate

        @active = true
      end

      def deactivate!
        @actions.remove_all_actions
      end

    end
    
    
    class CurseProxy
      
      # attr_reader :player
      
      def initialize(player, curse)
        @player = player
        @curse = curse
      end
      
      def temporary_effect(desc, amount, conditions = {})
        id = @player.stats.add_temporary_effect desc, amount, conditions
        @curse.actions.add_action id, :effect
      end

      def after_transition(args = {}, &block)
        id = @player.after_transition args, &block
        @curse.actions.add_action id, :state_transition
      end
      
    end
    
    

    class CurseActions
      def initialize(player)
        @player = player
        @actions = []
      end

      def empty?
        @actions.empty?
      end

      def add_action(id, type)
        @actions.push Action.new id, type, @player
      end

      def remove_all_actions
        @actions.each { |action| action.remove! }
        @actions = []
      end
    end

    class Action
      def initialize(id, type, player)
        @id = id
        @type = type
        @player = player
      end

      def remove!
        case @type
          when :effect
            @player.stats.remove_temporary_effect @id
          when :state_transition
            @player.remove_state_callback @id
          else
            nil
        end
      end
    end

  end
end