require 'manchkin/player_modules/proxy_classes/addition'
require 'manchkin/support/callbackable_methods'
require 'manchkin/player_modules/proxy_classes/restriction'


module Manchkin

    module PlayerModule

        class PlayerCard # карта на руках у игрока: сама карта + доп поля(как попала + прикрепленные карты)

            include Manchkin::Support::Delegator
            include Manchkin::Support::CallbackableMethods


            delegate_methods :bonus, :restrictions, :size, :gear_type,
                             :hand, :additions, :klass, :subtype,
                             to: :card

            attr_reader :card, :state, :player_cards_instance

            def initialize(player, card)
                @player = player
                @card = card
                @player_cards_instance = player.stats.player_cards
                @effects = []
                @actions = []
                @callbacks = []
                @state = :hand # :hand, :carried, :equipped
                add_card_global_actions
            end

            callbackable_method_define name: :is_active do |player_card_instance|
                # p player_card_instance.card.name
                # proxy = Manchkin::PlayerModule::RestrictionProxy.new player_card_instance.player_cards_instance, player_card_instance, true
                # if player_card_instance.card.respond_to? :restrictions
                #     pr = player_card_instance.restrictions
                #     proxy.instance_eval &pr unless pr.nil?
                #
                # end


                true
                # proxy.passed?
            end


            def carry!
                @state = :carried
                remove_added_methods [:carried]
                add_card_carry_actions
            end
            
            def equip!
                @state = :equipped
                remove_added_methods [:equipped]
                add_card_equip_actions
            end

            def add_temp_effect_id(id, card_states = [])
                @effects.push ::Manchkin::PlayerModule::CardAction.new(id, card_states)
            end
            
            def add_callback_id(id)
                @callbacks.push ::Manchkin::PlayerModule::CardAction.new(id)
            end

            def add_action_id(id, card_states = [])
                @actions.push ::Manchkin::PlayerModule::CardAction.new(id, card_states)
            end

            def remove_added_methods(states = [])
                remove_temp_effects states
                remove_callbacks
                remove_actions states
            end

            private

            def add_card_global_actions
                if @card.respond_to? :additions
                    proxy = Manchkin::PlayerModule::AdditionProxy.new @player, self
                    proxy.instance_eval &@card.additions unless @card.additions.nil?
                end
            end

            def add_card_carry_actions
                if @card.respond_to? :additions
                    proxy = Manchkin::PlayerModule::AdditionCarryProxy.new @player, self
                    proxy.instance_eval &@card.additions unless @card.additions.nil?
                end
            end

            def add_card_equip_actions
                if @card.respond_to? :additions
                    proxy = Manchkin::PlayerModule::AdditionEquippedProxy.new @player, self
                    proxy.instance_eval &@card.additions unless @card.additions.nil?
                end
            end

            def remove_temp_effects(states)
                @effects.each { |card_action| @player.stats.remove_temporary_effect(card_action.id) unless card_action.states_include_one_of states }
            end

            def remove_callbacks
                # @player.notification_message @callbacks, @player.id
                @callbacks.each { |card_action| @player.remove_callback id: card_action.id }
            end

            def remove_actions(states)
                @actions.each { |card_action| @player.remove_dinamic_action(id: card_action.id) unless card_action.states_include_one_of states }
            end

        end

        class CardAction

            attr_reader :id, :states

            def initialize(id, states = [])
                @id = id
                @states = states.is_a?(Array) ? states : [states]
            end

            def states_include_one_of(states_array)
                states_array.each do |state|
                    return true if @states.include? state
                end
                false
            end
        end
    end
end