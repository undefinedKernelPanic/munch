require 'manchkin/support/callbackable_methods'
require "manchkin/support/delegator"
require "manchkin/support/temporary_effect"
require "manchkin/support/dinamic_actions"

require 'manchkin/player_modules/proxy_classes/restriction'
require 'manchkin/player_modules/player_cards'
require 'manchkin/player_modules/klass'
require 'manchkin/player_modules/race'



module Manchkin
    module PlayerModules
			class PlayerStats

				# KLASS_ABILITIES = [:rage]

				include Manchkin::Support::CallbackableMethods
				include Manchkin::Support::DinamicActions
				include Manchkin::Support::Delegator

				delegate_methods :notification_message, to: :player
				delegate_methods :equip, :power, :add_to_hand, :add_cards, :hand,
												 :carried, :equipped, :find_player_card_by_card_id,
												 :drop_card, :put_to_desk, :maximum_big_gear,
												 :maximum_cards_at_end_turn,
												 to: :player_cards

				delegate_methods :klass, :klass_name, to: :klass_object
				delegate_methods :race, :race_name, to: :race_object

				attr_reader :player, :sex, :temporary_effects, :player_cards

				def initialize(player, sex)
					@sex = sex
					@player = player
					@klass = Manchkin::PlayerModule::PlayerKlass.new player
					@race = Manchkin::PlayerModule::PlayerRace.new player
					@level = 1
					@temporary_effects = Manchkin::Support::TemporaryEffects.new player
					@player_cards = Manchkin::PlayerModule::PlayerCards.new player
				end

				def set_level(new_level)
					@level = new_level
				end

				def sex=(new_sex)
					@sex = new_sex
				end

				def race_object
					@race
				end

				def klass_object
					# '@klass'
					@klass
				end

				def add_temporary_effect(description, amount, conditions = {})
					@temporary_effects.add description, amount, nil, conditions
				end

				def remove_temporary_effect(id)
					@temporary_effects.remove_by_id id
				end

				callbackable_method_define name: :level do |stat_instance|
					stat_instance.instance_variable_get "@level"
				end


		end

	end
end