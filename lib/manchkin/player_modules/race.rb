require 'manchkin/support/delegator'


module Manchkin

  module PlayerModule

    class PlayerRace

      attr_reader :player, :player_action_ids, :player_card_race

      def initialize(player)
        @player = player
        @race_name = :human

        @player_action_ids = []
        @player_card_race = nil

      end

      def race
        self
      end

      def race_name
        @race_name
      end

      def equip_race!(player_card)
        unless @player_card_race.nil?
          @player_card_race.carry!
        end
        @player_card_race = player_card
        @player_card_race.equip!

        @race_name = player_card.card.race_name

        proxy = RaceProxy.new self
        proxy.instance_eval &player_card.card.race_abilities
      end

      def unequip_race!
        # p '***********************************************88888'
        @player_card_race.carry!
        @player_card_race = nil

        @race_name = :human
        remove_actions
      end

      private

      def remove_actions
        @player_action_ids.each do |id|
          @player.remove_callback id: id
        end
      end
    end


    class RaceProxy

      def initialize(player_race_inst)
        @player_klass = player_race_inst
      end

      def callback(mod, method_name, is_permanent, &imp)
        id = @player_klass.player.add_callback mod, method_name, is_permanent, &imp
        @player_klass.player_action_ids.push id
      end

    end
  end
end