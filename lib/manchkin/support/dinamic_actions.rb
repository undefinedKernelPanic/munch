module Manchkin
    module Support
        module DinamicActions

            alias_method :original_method_missing, :method_missing

            def method_missing(m, *args, &block)
                method = available_dinamic_actions.find { |action| action.action_name == m }
                if method.nil?
                    p 'нельзя ща'
                    original_method_missing m, *args, &block

                else
                    # p 'ooooooooooooooooo'
                    # p method
                    # p method
                    method.call self, args
                end
            end

            def respond_to_missing?(method, include_private = false)
                available_dinamic_actions.map(&:action_name).include? method
            end



            def add_dinamic_action(action_name, conditions = {}, &block)
                new_action = DinamicAction.new self, action_name, conditions, block
                dinamic_actions.push new_action
                new_action.id
            end

            def remove_dinamic_action(args = {}) # id, name
                key = :id
                value = args[:id]
                # p '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111'
                # p key, value
                dinamic_actions.delete_if { |action| action.send(key) == value }
            end

            def available_dinamic_actions
                # p '?????????????????????????????????????'
                # p dinamic_actions.select { |action| action.avaible_for_now? }
                dinamic_actions.select { |action| action.avaible_for_now? }
                # dinamic_actions
            end
            
            def available_dinamic_actions_names
                available_dinamic_actions.map &:action_name
            end

            def self.included(base = nil, &block)
                base.instance_eval do
                    define_method :dinamic_actions do # возвращает все динамически добавленные экшены
                    unless instance_variable_defined?("@dinamic_actions")
                        instance_variable_set "@dinamic_actions", []
                    end
                        instance_variable_get "@dinamic_actions"
                    end
                end
            end
        end

        class DinamicAction

            attr_reader :action_name, :id

            def initialize(method_owner, action_name, conditions, block)
                @action_name = action_name
                @conditions = conditions
                @action = block
                @id = IdGenerator.generate_id
                @method_owner = method_owner
            end

            def call(caller, *args)
                @action.call caller, args
            end

            def avaible_for_now?
                @conditions.each do |condition, value|
                    val = value
                    unless val.is_a? Array
                        val = [val]
                    end
                    return false unless val.include? @method_owner.send(condition)
                end
                true
            end
        end
    end
end