module Manchkin
    module Support
        module ToJSON
            def to_json(args = []) # by default full information about player, :other - только то, что игрок может знать о других игроках
                if args.include? :other
                    transform_to_json self.class.const_get 'TO_JSON_FIELDS_OTHER'
                else
                    transform_to_json self.class.const_get 'TO_JSON_FIELDS_OWNER'
                end
            end

            private
 
            def transform_to_json(attributes)
            # p attributes
                result = {}
                attributes.each do |attr|
                    if attr.is_a? Hash
                        k = attr.first[0]
                        # p attr.first
                        v = instance_exec 3, &attr.first[1]
                        result[k] = v
                    else
                        result[attr] = send(attr)
                    end
                end
                result.to_json
                
            end
        end
    end
end