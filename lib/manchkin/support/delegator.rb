#TODO придумать ченить с respond_to?
module Manchkin
    module Support
        module Delegator

            private

            def self.included(base)
                base.extend ClassMethods
            end

        end

        module ClassMethods
            def delegate_methods(*attrs)
                options = extract_options! attrs
                unless association = options[:to]
                    raise 'need specify a target'
                end

                attrs.each do |method_name|
                    define_method method_name do |*attrs|
                        public_send(association).public_send(method_name, *attrs)
                    end
                end

            end

            private

            def extract_options!(attrs)
                attrs.delete(attrs.find { |item| item.is_a? Hash }) || {}
            end
        end
    end
end