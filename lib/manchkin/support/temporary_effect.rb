module Manchkin
  module Support


    class TemporaryEffects

      attr_reader :effects
      def initialize(player)
        @player = player
        @effects = []
      end

      def add(description, amount, tag, conditions = {}, &block)
        new_effect = Effect.new @player, description, amount, tag, conditions, &block
        @effects.push new_effect
        new_effect.id
      end
      def remove_by_id(id)
        @effects.delete_if { |b| b.id == id }
      end

      def amount(tag = nil)
        @effects.inject(0) { |res, bonus| (bonus.pass_conditions? && bonus.tag == tag) ? res + bonus.amount : res }
      end

      def active_effects(tag = nil)
        @effects.select { |bonus| bonus.pass_conditions? && bonus.tag == tag }
      end

      # def select_by_tag(tag)
      #   @effects.select { |effect| effect.tag == tag }
      # end
    end

    class Effect

      attr_reader :description, :amount, :id, :tag

      def initialize(player, description, amount, tag, conditions = {}, &block)

        raise 'need desc and amount' if description.nil? || amount.nil?

        @id = IdGenerator.generate_id
        @description = description
        @amount = amount
        @conditions = conditions
        @player = player
        @tag = tag
        @conditions_checker = block
      end

      def pass_conditions?
        @conditions.each do |k, v|
          if @conditions_checker.nil?
            return false unless @player.send(k.to_sym) == v
          else
            return false unless @conditions_checker.call k, v
          end
        end
        true
      end
    end

  end
end