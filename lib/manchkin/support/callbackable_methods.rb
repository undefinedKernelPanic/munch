module Manchkin
  module Support
    module CallbackableMethods

      def remove_callback(args = {}) # id
        @defined_methods.each do |method|
          method.remove_action args[:id]
        end
      end
  
      def add_callback(mod, method_name, is_permanent = false, &callback)
          # p 'dghghdh'
          # p method_name
          # p respond_to? :"#{method_name}_callbackable"
          inst_var = public_send :"#{method_name}_callbackable"
          inst_var.public_send :"add_#{mod}_action", is_permanent, callback
      end

      private

      def self.included(base)

        super
        # p descendant

        base.instance_eval do

          @@default_delegate_to = nil
          # @@default_confirmation_method = nil

          def callbackable_methods_set_defaults(args = {}) # delegated_to: :(method that return object that respond to :method_name)
            @@default_delegate_to = args[:delegate_to]
            # @@default_confirmation_method = args[:confirmation_method]
          end	

          def callbackable_method_define(args = {}, &block) # name: :method_name,
                                                    # delegated_to: :(method that return object that respond to :method_name)
                                                    # or code block for method declaration
            raise 'need specify method name' if args[:name].nil?

            method_define args[:name]
            define_instanse_variable args[:name], args[:delegate_to] || @@default_delegate_to, &block


          end

          private

          def define_instanse_variable(method_name, delegated_to, &block)
            define_method :"#{method_name}_callbackable" do
              unless instance_variable_defined?("@#{method_name}_callbackable")
                method_emp = block_given? ? (-> (obj, *args) { yield(obj, args) }) : (-> (obj, *args) { obj.public_send(delegated_to).public_send(method_name, obj, *args) })
                action = BaseAction.new self, method_name, method_emp
                self.instance_variable_set "@#{method_name.to_s}_callbackable", action
              end

              instance_variable_set(
                "@defined_methods",
                (instance_variable_get("@defined_methods") || []).push(instance_variable_get("@#{method_name}_callbackable"))
              )

              instance_variable_get "@#{method_name}_callbackable"
            end
            # TODO refactor this shit


          end

          def method_define(method_name)
            define_method method_name do |*args|	
              self.public_send(:"#{method_name}_callbackable").call *args
          end	
        end
      end
      end
    
    end

    class BaseAction
      class Action

        attr_reader :id

        def initialize(action, is_permanent)
          @id = IdGenerator.generate_id
          # @id = 2
          @action = action
          @is_permanent = is_permanent
        end

        def call(object, action_result = nil, *args)
          @action.call object, action_result, *args
        end

        def permanent?
          @is_permanent
        end
      end

      def initialize(object, method_name, method_emp)
        @method_name = method_name
        @object = object
        @method_emp = method_emp
        @before = []
        @instead = nil
        @after = []
        @action_result = nil
      end

      def call(*args)
        run_before_actions
        run_instead_action *args
        res = run_after_actions

        # p '3232323333333333333333333333333333333333333333'
        # p res

        res || @action_result


        
        # @action_result
      end

      def add_before_action(is_permanent = false, action)
        new_action = Action.new(action, is_permanent)
        # p new_action
        @before.push new_action
        # p @before

        new_action.id

      end

      def add_after_action(is_permanent = false, action)
        new_action = Action.new(action, is_permanent)
        @after.push new_action
        new_action.id
      end

      def add_instead_action(is_permanent = false, action)
        new_action = Action.new(action, is_permanent)
        @instead = new_action
        new_action.id
      end

    #  def all_callbacks
    #  	(@before + @after)
      # end

      def remove_action(action_id)
        @after.delete_if { |action| action.id == action_id }
        @before.delete_if { |action| action.id == action_id }
        @instead = nil if @instead&.id == action_id
      end

      private

      def run_before_actions
        @before.each do |action|
          action.call @object
          # unless action&.permanent?
          #   @before.delete action
          # end
        end
        @before.delete_if { |action| !action.permanent? }

      end

      def run_after_actions(result = @action_result)
        init_res = result
        p @after if @after.size > 1
        @after.each do |action|
          # p '555555555555555555555555555'
          # p @after
          # p init_res
          init_res = action.call @object, init_res
          # unless action&.permanent?
          #   @after.delete action
          # end
        end

        @after.delete_if { |action| !action.permanent? }
        init_res
        # @after = []
      end



      def run_instead_action(*args)
        # p '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111'
        # p @instead.nil?
        @action_result = @instead.nil? ? @method_emp.call(@object, *args) : @instead.call(@object, *args)
        unless @instead&.permanent? # все норм, безопасный оператор &.
          @instead = nil
        end
      end
    end

  end
end