

class TestCardFactory < BasicObject
  def initialize
    @attributes = {}
  end

  attr_reader :attributes

  def method_missing(name, *args, &block)
    attributes[name] = args[0]
  end
end

def card(card_class, &block)
	card_factory = TestCardFactory.new
	if block_given?
	  card_factory.instance_eval &block
	end
	card_instance = Object.const_get("Manchkin::#{card_class.capitalize}").allocate

	card_factory.attributes.each do |attr_name, attr_val|
	  card_instance.instance_variable_set :"@#{attr_name}", attr_val
	  card_instance.class.class_eval { send :attr_reader, :"#{attr_name}" }
	end

	card_instance.instance_variable_set :@card_type, card_instance.card_type
	card_instance.instance_variable_set :@subtype, card_instance.class.to_s.gsub('Manchkin::', '').to_sym.downcase
	card_instance.instance_variable_set :@id, card_instance.object_id
	card_instance.class.class_eval { send :attr_reader, :card_type, :subtype }
	card_instance.class.class_eval { send :attr_reader, :subtype }


	card_instance
end


player = Manchkin::Player.new 'leo', :male

player_cards = Manchkin::PlayerModule::PlayerCards.new player

card_equip_action_condition = card :gear do
	name 'башмаки реально быстрого бега'
	gear_type :boots
	bonus 3
	price 400
	size :small
	add = Proc.new do
		action_if_equip :equip_action do |player, card_source_of_action, monster_char_id|
			p 'some_action!!!!'
		end
	end
	additions add
	# e = Proc.new do
	# 	# except profession: :thief
	# 	only klass: :mage
	# end
	# restrictions e
end

card_carry_action_condition = card :gear do
	name 'башмаки реально быстрого бега'
	gear_type :boots
	bonus 3
	price 400
	size :small
	add = Proc.new do
		action_if_carry :some_action do |player, card_source_of_action, monster_char_id|
			p 'some_action!!!!'
		end
	end
	additions add
end

card_global_action = card :gear do
	name 'башмаки реально быстрого бега'
	gear_type :boots
	bonus 3
	price 400
	size :small
	add = Proc.new do
		have_action :drop_rat do |player, card_source_of_action, monster_char_id|
			p 'DROP!!!!'
		end
	end
	additions add
end

card_global_action_condition = card :gear do
	name 'башмаки реально быстрого бега'
	gear_type :boots
	bonus 3
	price 400
	size :small
	add = Proc.new do
		have_action :drop_boots, state: Manchkin::PlayerOpenDoor.instance do |player, card_source_of_action, monster_char_id|
			p 'DROP!!!!'
		end
	end
	additions add
end


big_chest = card :gear do
	name 'мифрильный панцирь'
	gear_type :chest
	size :big
	bonus 3
	price 600
end

FIXTURES = {
    player: player,
    player_cards: player_cards,
    big_chest: big_chest,
		card_global_action: card_global_action,
		card_global_action_condition: card_global_action_condition,
		card_carry_action_condition: card_carry_action_condition,
		card_equip_action_condition: card_equip_action_condition
}