require 'spec_helper'

describe Manchkin::PlayerModule::PlayerCards do
  describe '#add_to_hand' do
    let(:player) { Manchkin::Player.new 'leo', :male }
    let(:player_card) { Manchkin::PlayerModule::PlayerCards.new player }
    let(:card) { FIXTURES[:big_chest] }
    
    
    subject { player_card.add_to_hand card }
    
    it 'card should be added' do
      subject
      expect(player_card.all_cards.size).to eq 1
    end
    
    context 'when have global addition' do
      let(:player) { Manchkin::Player.new 'leo', :male }
      let(:player_card) { Manchkin::PlayerModule::PlayerCards.new player }
      let(:card) { FIXTURES[:card_global_action] }

      context 'card have not conditions' do
        let(:card) { FIXTURES[:card_global_action] }
        it 'player should can call action' do
          subject
          expect(player).to respond_to(:drop_rat)
        end
      end

      context 'card have conditions' do
        let(:card) { FIXTURES[:card_global_action_condition] }
        it 'player should can call action' do
          subject
          expect(player).not_to respond_to(:drop_boots)
        end
      end
    end
  end

  describe '#put_to_desk' do
    let(:player) { Manchkin::Player.new 'leo', :male }
    let(:card) { FIXTURES[:card_carry_action_condition] }
    # let(:player_card) { Manchkin::PlayerModule::PlayerCard.new player, card }
    let(:player_cards) { Manchkin::PlayerModule::PlayerCards.new player }

    let(:player_card) { player_cards.add_to_hand card }

    subject { player_cards.put_to_desk player_cards.all_cards.first }

    it 'card should be put on desk' do
      player_cards.add_to_hand card
      subject
      # player_cards.put_to_desk player_cards.all_cards.first
      expect(player_cards.all_cards.size).to eq 1
      expect(player_cards.all_cards.first.state).to eq :carried
      expect(player).to respond_to :some_action
    end
  end

  describe '#equip' do
    let(:player) { Manchkin::Player.new 'leo', :male }
    let(:card) { FIXTURES[:card_equip_action_condition] }
    # let(:player_card) { Manchkin::PlayerModule::PlayerCard.new player, card }
    let(:player_cards) { Manchkin::PlayerModule::PlayerCards.new player }

    let(:player_card) { player_cards.add_to_hand card }

    subject { player_cards.equip player_cards.all_cards.first }

    it 'card should be equipped' do
      player_cards.add_to_hand card
      subject
      # player_cards.put_to_desk player_cards.all_cards.first
      expect(player_cards.all_cards.size).to eq 1
      expect(player_cards.all_cards.first.state).to eq :equipped
      expect(player).to respond_to :equip_action

      p player_cards.power

      player_cards.put_to_desk player_cards.all_cards.first

      p player_cards.power


      expect(player).not_to respond_to :equip_action

    end
  end

end