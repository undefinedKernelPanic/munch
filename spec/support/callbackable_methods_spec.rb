require 'spec_helper'

describe Manchkin::Support::BaseAction do
    let(:callback_imp) { -> { return 'callback_imp' } }
    let(:instance) { Class.new.new }
    let(:method_implementation) { -> { return 'wowow' } }
    let(:base_action) { Manchkin::Support::BaseAction.new instance, :do_something, method_implementation }
    describe '.add_before_action' do
        it 'before callback should be added' do
            id = base_action.add_before_action callback_imp
            expect(id).not_to eq nil
            expect(base_action.instance_variable_get('@before').size).to eq 1 
        end
    end
    
    describe '.add_before_action' do
        it 'before callback should be added' do
            id = base_action.add_before_action callback_imp
            expect(id).not_to eq nil
            expect(base_action.instance_variable_get('@before').size).to eq 1 
        end
    end
    
    describe '.add_after_action' do
        it 'after callback should be added' do
            id = base_action.add_after_action callback_imp
            expect(id).not_to eq nil
            expect(base_action.instance_variable_get('@after').size).to eq 1 
        end
    end
    
    describe '.add_instead_action' do
        it 'instead callback should be added' do
            id = base_action.add_instead_action callback_imp
            expect(id).not_to eq nil
            expect(base_action.instance_variable_get('@instead')).not_to eq nil  
        end
    end
    
    describe '.remove_action' do
        let(:base_action) { Manchkin::Support::BaseAction.new instance, :do_something, method_implementation }
    
        let(:before_callback_id) { base_action.add_before_action callback_imp }
        let(:after_callback_id) { base_action.add_after_action callback_imp }
        let(:instead_callback_id) { base_action.add_instead_action callback_imp }
        it 'callback should be deleted' do
            base_action.remove_action before_callback_id
            base_action.remove_action after_callback_id
            base_action.remove_action instead_callback_id
            
            expect(base_action.instance_variable_get('@instead')).to eq nil
            expect(base_action.instance_variable_get('@before').size).to eq 0
            expect(base_action.instance_variable_get('@after').size).to eq 0
        end
    end
    describe '.call' do
    
        context 'with instead' do
            let(:base_action) { Manchkin::Support::BaseAction.new instance, :do_something, method_implementation }
            
            let(:before_callback_imp) { -> (obj, args) { p obj, args } }
            let(:after_callback_imp) { -> (obj, args) { p obj, args } }
            let(:instead_callback_imp) { -> (obj, args) { p obj } }
            
            it 'callbacks should be ran' do
                base_action.add_before_action before_callback_imp
                base_action.add_after_action after_callback_imp
                base_action.add_instead_action instead_callback_imp
                
                base_action.call :j
                
                # expect(base_action.call).to output('before_callback_imp').to_stdout
                expect(true).to eq true
             
            end
            
        end
                
            
    end
    
end

describe Manchkin::Support::CallbackableMethods do
    describe '.included' do
        let(:included_class) { Class.new { include Manchkin::Support::CallbackableMethods } }
    
        it 'class respond to callbackable_method_define' do
            expect(included_class.respond_to? :callbackable_method_define).to eq true
        end
        it 'class respond to callbackable_methods_set_defaults' do
            expect(included_class.respond_to? :callbackable_methods_set_defaults).to eq true
        end
        
        it 'instance respond to remove_callback' do
            expect(included_class.new.respond_to? :remove_callback).to eq true            
        end
        
        it 'instance respond to add_callback' do
            expect(included_class.new.respond_to? :add_callback).to eq true            
        end
    end
    
    
    
end